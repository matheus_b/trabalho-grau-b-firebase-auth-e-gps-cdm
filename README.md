# cadastro_localizacao

## Rodando o projeto
• Instale o plugin do Flutter no Android Studio:


 - File


 - Settings


 - Plugins


 - Flutter


## Baixe o Flutter
 - Acesse https://flutter.dev/docs/get-started/install/windows


 - Baixe o .zip e extraia


 - Coloque o caminho completo da pasta bin acima como variável de ambiente do Windows



 No meu caso, coloquei o seguinte caminho como variável de ambiente no Path:
 C:\dev\flutter\bin


## Defina o caminho do SDK do Dart no Android Studio
O próprio Flutter já traz o SDK do Dark incluso no .zip baixado acima, dentro de bin -> cache -> dart-sdk


 - File
 
 
 - Settings
 
 
 - Languages & Frameworks
 
 
 - Dart
 
 
 - Defina o path para [caminho completo]\flutter\bin\cache\dart-sdk


 No meu caso, ficou assim:
 C:\dev\flutter\bin\cache\dart-sdk


## Instale as dependências/libs do app
 
 
 - Abra o arquivo pubspec.yaml
 
 
 - Clique em "packages get"


## Dando play no app
 É necessário setar o main.dart para dar play em um dispositivo.
 
 
 - Edit configurations
 
 
 - Em "Dart Entrypoint", coloque o caminho completo para o main.dart do app
 
 
 - Dê play para rodar o app no dispositivo e aproveite a vantagem do hot reload do Flutter! :)


## Gerar versão
 Rode os comandos abaixo para gerar um .apk ou uma versão para iOS, respectivamente:
 
 
 flutter build apk
 
 
 flutter build ios