import 'package:cadastro_localizacao/src/screens/create_account_screen.dart';
import 'package:cadastro_localizacao/src/screens/save_location_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cadastro_localizacao/src/globals.dart' as globals;

class LoginScreen extends StatefulWidget {
  @override
  State createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            _background(),
            Container(
              height: MediaQuery.of(context).size.height,
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.3),
              child: _form(),
            ),
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.2),
                child: Column(
                  children: <Widget>[
                    Text(
                      'Bem-vindo!',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 32,
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _form() {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          _formInput(
              'E-mail',
              false,
              Icon(
                Icons.person,
                color: Colors.white,
              ),
              _emailController),
          _formInput(
              'Senha',
              true,
              Icon(
                Icons.lock,
                color: Colors.white,
              ),
              _passwordController),
          _formSubmitButton(),
          _registerButton()
        ],
      ),
    );
  }

  _formSubmitButton() {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.76,
      child: RaisedButton(
        elevation: 0,
        highlightElevation: 0,
        color: Color.fromRGBO(255, 255, 255, 0.25),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        onPressed: () {
          if (_formKey.currentState.validate() && !isLoading) {
            setState(() {
              isLoading = true;
            });

            firebaseAuth
                .signInWithEmailAndPassword(
                    email: _emailController.text,
                    password: _passwordController.text)
                .then((AuthResult user) {
              setState(() {
                isLoading = false;
              });

              globals.id = user.user.uid;
              globals.email = user.user.email;

              Navigator.pushReplacement(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (c, a1, a2) => SaveLocationScreen(),
                  ));
            }).catchError((error) {
              setState(() {
                isLoading = false;
              });

              showDialog(
                barrierDismissible: false,
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    contentPadding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.05,
                      horizontal: 13,
                    ),
                    content: Container(
                      padding: EdgeInsets.all(5),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            error.message,
                            style: TextStyle(
                              fontSize:
                              MediaQuery.of(context).size.height * 0.0235,
                            ),
                          ),
                        ],
                      ),
                    ),
                    actions: <Widget>[
                      FlatButton(
                        child: Text(
                          'OK',
                          style: TextStyle(
                            fontFamily: 'HindMedium',
                            height: 0.8,
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
            });
          }
        },
        child: !isLoading
            ? Text(
                "Entrar",
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  color: Colors.white,
                  fontSize: 15,
                ),
              )
            : SizedBox(
                width: 20,
                height: 20,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  strokeWidth: 3,
                ),
              ),
      ),
    );
  }

  _registerButton() {
    return Container(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.12),
      child: Column(
        children: <Widget>[
          Text(
            'Ainda não tem uma conta?',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.60,
            child: RaisedButton(
              elevation: 0,
              highlightElevation: 0,
              color: Color.fromRGBO(255, 255, 255, 0.25),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
              onPressed: () {
                Navigator.push(
                    context,
                    PageRouteBuilder(
                      pageBuilder: (c, a1, a2) => CreateAccountScreen(),
                    ));
              },
              child: Text(
                "Cadastre-se",
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  color: Colors.white,
                  fontSize: 15,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  _formInput(label, isObscure, icon, textEditingController) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.1, vertical: 10),
      padding: EdgeInsets.fromLTRB(10, 3, 10, 3),
      child: TextFormField(
          obscureText: isObscure,
          controller: textEditingController,
          validator: (value) => _validateNotEmptyOrNullOrFalse(value),
          style: TextStyle(color: Colors.white),
          decoration: InputDecoration(
            hintText: label,
            hintStyle: TextStyle(color: Colors.white),
            contentPadding: EdgeInsets.fromLTRB(20, 13, 13, 13),
            prefixIcon: icon,
            border: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(30)),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(30)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(30)),
          )),
    );
  }

  _background() {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('./assets/img/bg.jpg'),
            fit: BoxFit.cover,
            alignment: Alignment.centerLeft),
      ),
    );
  }

  String _validateNotEmptyOrNullOrFalse(value) {
    if (value == '' || value == null || value == false) {
      return 'Obrigatório';
    } else {
      return null;
    }
  }
}
