import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cadastro_localizacao/src/globals.dart' as globals;
import 'package:intl/intl.dart';


class HistoryLocationScreen extends StatefulWidget {
  @override
  State createState() => HistoryLocationScreenState();
}

class HistoryLocationScreenState extends State<HistoryLocationScreen> {
  DatabaseReference positionDatabaseReference =
      FirebaseDatabase.instance.reference().child("posicoes");
  List<dynamic> list = [];
  List<dynamic> listByUser = [];
  List<dynamic> filteredList = [];

  @override
  void initState() {
    super.initState();
    positionDatabaseReference
        .orderByChild("datetime")
        .limitToLast(5)
        .once()
        .then((DataSnapshot result) {
      if (result.value != null) {
        result.value.forEach((index, value) {
          setState(() {
            list.add(value);
          });
        });

        list.forEach((item) {
          if (item['userId'] == globals.id) {
            listByUser.add(item);
          }
        });

        listByUser = listByUser.reversed.toList();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            _background(),
            Container(
              height: MediaQuery.of(context).size.height,
              alignment: Alignment.center,
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.15),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).size.height * 0.1),
                    child: Text(
                      'Histórico de Localização',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                      ),
                    ),
                  ),
                  listByUser.length > 0
                      ? ListView.builder(
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          itemCount: listByUser.length,
                          itemBuilder: (BuildContext context, int index) {
                            return _showItem(listByUser[index]);
                          },
                        )
                      : Container(),
                  _backButton(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _backButton() {
    return Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.1),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.60,
        child: RaisedButton(
          elevation: 0,
          highlightElevation: 0,
          color: Color.fromRGBO(255, 255, 255, 0.25),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            "Voltar",
            style: TextStyle(
              fontWeight: FontWeight.w400,
              color: Colors.white,
              fontSize: 15,
            ),
          ),
        ),
      ),
    );
  }

  _background() {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('./assets/img/bg.jpg'),
            fit: BoxFit.cover,
            alignment: Alignment.centerLeft),
      ),
    );
  }

  _showItem(values) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
            padding: EdgeInsets.only(bottom: 10),
            child: Text(
              '${values["latitude"]}, ${values["longitude"]} às '
              '${DateFormat('HH:mm:ss - dd/MM/yyyy').format(DateTime.parse(values["datetime"]))}',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            )),
      ],
    );
  }
}
