import 'dart:async';

import 'package:cadastro_localizacao/src/screens/history_location_screen.dart';
import 'package:cadastro_localizacao/src/screens/login_screen.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cadastro_localizacao/src/globals.dart' as globals;

class SaveLocationScreen extends StatefulWidget {
  @override
  State createState() => SaveLocationScreenState();
}

class SaveLocationScreenState extends State<SaveLocationScreen> {
  LocationData currentLocation;
  var location = new Location();
  String error;
  DatabaseReference positionDatabaseReference =
      FirebaseDatabase.instance.reference().child("posicoes");
  Timer _timer;
  bool isCounting = false;
  int _start = 300;
  int minutes = 0;
  int seconds = 0;
  String currentDatetime;

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            _background(),
            Container(
              height: MediaQuery.of(context).size.height,
              alignment: Alignment.center,
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.15),
              child: Column(
                children: <Widget>[
                  Text(
                    'Salvar localização',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 32,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 30, bottom: 20),
                    child: Text(
                      'Clique no botão abaixo para salvar a sua localização a cada 5 minutos.',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  _saveButton(),
                  isCounting ? _showTimer() : Container(),
                  _historyButton(),
                  _logout()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _saveButton() {
    return Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.07),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.60,
        child: RaisedButton(
          elevation: 0,
          highlightElevation: 0,
          color: Color.fromRGBO(255, 255, 255, 0.25),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          onPressed: () async {
            if (isCounting) {
              setState(() {
                minutes = 0;
                seconds = 0;
                _start = 300;
                isCounting = false;
              });
              _timer.cancel();
            } else {
              setState(() {
                isCounting = true;
              });
              startTimer();
              _sendLocationToFirebase();
            }
          },
          child: isCounting
              ? Text(
                  "Cancelar",
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                    fontSize: 15,
                  ),
                )
              : Text(
                  "Salvar posição",
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                    fontSize: 15,
                  ),
                ),
        ),
      ),
    );
  }

  _historyButton() {
    return Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.60,
        child: RaisedButton(
          elevation: 0,
          highlightElevation: 0,
          color: Color.fromRGBO(255, 255, 255, 0.25),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          onPressed: () {
            setState(() {
              minutes = 0;
              seconds = 0;
              _start = 300;
              isCounting = false;
            });
            if (_timer != null) {
              _timer.cancel();
            }

            Navigator.push(
                context,
                PageRouteBuilder(
                  pageBuilder: (c, a1, a2) => HistoryLocationScreen(),
                ));
          },
          child: Text(
            "Histórico de Localização",
            style: TextStyle(
              fontWeight: FontWeight.w400,
              color: Colors.white,
              fontSize: 15,
            ),
          ),
        ),
      ),
    );
  }

  _logout() {
    return Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.60,
        child: RaisedButton(
          elevation: 0,
          highlightElevation: 0,
          color: Color.fromRGBO(255, 255, 255, 0.25),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          onPressed: () {
            globals.id = null;
            globals.email = null;

            Navigator.pushReplacement(
                context,
                PageRouteBuilder(
                  pageBuilder: (c, a1, a2) => LoginScreen(),
                ));
          },
          child: Text(
            "Sair",
            style: TextStyle(
              fontWeight: FontWeight.w400,
              color: Colors.white,
              fontSize: 15,
            ),
          ),
        ),
      ),
    );
  }

  _background() {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('./assets/img/bg.jpg'),
            fit: BoxFit.cover,
            alignment: Alignment.centerLeft),
      ),
    );
  }

  _showTimer() {
    return Container(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.07),
      child: Column(
        children: <Widget>[
          currentLocation != null
              ? Text(
                  'Última captura: \n${currentLocation.latitude}, ${currentLocation.longitude} às '
                '${DateFormat('HH:mm:ss - dd/MM/yyyy').format(DateTime.parse(currentDatetime))}',
                  style: TextStyle(color: Colors.white),
                  textAlign: TextAlign.center,
                )
              : Container(),
          Text(
            '\nA próxima captura será em $minutes:$seconds',
            style: TextStyle(color: Colors.white),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }

  _sendLocationToFirebase() async {
    try {
      currentLocation = await location.getLocation();
      setState(() {
        currentDatetime = DateTime.now().toIso8601String();
      });
      positionDatabaseReference.push().set(<String, String>{
        "userId": globals.id,
        "latitude": currentLocation.latitude.toString(),
        "longitude": currentLocation.longitude.toString(),
        "datetime": currentDatetime
      });
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'Permission denied';
      }
      currentLocation = null;
    }
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(oneSec, (Timer timer) {
      setState(() {
        _start--;
      });
      try {
        setState(() {
          if (_start < 1) {
            _sendLocationToFirebase();
            setState(() {
              _start = 300;
            });
          } else {
            minutes = (_start ~/ 60).toInt();
            seconds = _start % 60;
          }
        });
      } catch (e) {
        timer.cancel();
      }
    });
  }
}
